'use strict';

var room              = room,
    roomsList         = [],
    peers             = {},
    users             = {},
    protocol          = window.location.protocol,
    hostname          = window.location.hostname,
    rootUrl           = protocol + "//" + hostname,
    localStream       = undefined,
    remoteStream      = undefined,
    localScreen       = undefined,
    id                = null/*,
    daddy             = window.parent*/;
    

var onBistriConferenceReady = function()
{
    bc.init( {
        appId              : window.parent.appId,
        appKey             : window.parent.appKey,
        debug              : false
    });

    bc.signaling.bind( "onConnected", function( data )
    {
        /*daddy.getElementById("connecting").style.fontWeight="normal";
        daddy.getElementById("waitingcamera").style.fontWeight="bold";*/
        id = data.id;
        
        bc.startStream( "webcam-hd", function( stream, pid )
        {
            localStream = stream;
            
            var room = testId;
            console.log("Room:", room)
            bc.joinRoom( room );
            
        }.bind(this) );
    } );

    bc.signaling.bind( "onJoinedRoom", function( data )
    {
        if(data.members.length){
            bc.call(data.members[0].id, data.room, {stream: localStream});
            //bc.openDataChannel( data.members[ 0 ].id, "chat", data.room );
        }
        else alert("Error: server not present in room 2")
    } );
    
    bc.streams.bind ( "onStreamAdded", function( stream, pid )
    {
        remoteStream = stream;
        bc.attachStream ( stream, document.getElementById('video') );
        bc.monitorAudioActivity( remoteStream, function( audioLevel ){                     
            document.getElementById('vuminside').style.height = (100 - audioLevel) + "px";
        } );
    } );

    bc.signaling.bind( "onPeerJoinedRoom", function( data )
    {
        if ( !users[ data.pid ] )
        {
            users[ data.pid ] = {
                id: data.pid,
                name: data.name || 'unknown'
            };
        }
    } );
    
    bc.signaling.bind( "onDisconnected", function( data )
    {
        bc.stopStream (localStream, function( stream ) {
            bc.detachStream( remoteStream );
        } );
    } );

    bc.signaling.bind( "onPeerQuittedRoom", function( data )
    {
        bc.disconnect();
    } );

    bc.streams.bind( "onStreamError", function( error ){
        alert("Error:", error);
    } );    
    
    bc.channels.bind( "onDataChannelRequested", function ( dataChannel, remoteUserId ){
    });
    
    bc.channels.bind( "onDataChannelCreated", function ( dataChannel, remoteUserId ){
        dataChannel.onMessage = function( event ){
        };
    });
};
