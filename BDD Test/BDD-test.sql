-- Création de la base de données
-- Si la bdd existe déjà sur le serveur, on l‘éfface  
DROP DATABASE IF EXISTS cartable_connecte;
CREATE DATABASE cartable_connecte;
USE cartable_connecte;

-- Création de la table test réseau 
-- id: entier non signé qui s‘auto incrémente et il doit être unique
-- test_datetime: c'est une date qui permet de savoir quand un test a été éffectué
-- operateur: nom de opérateur ex:ORANGE, FREE, SFR ...
-- download: décimal (5 chifres,2chiffres) de téléchargement
-- upload: décimal (5 chifres,2chiffres) d'envoie de données
-- debit_resultat: caractères (10 max) qui dit si le test de débit est ok ou échec (échec par defaut)
-- mirroir_resultat: caractères (10 max) qui dit si le test mirroir est ok ou échec (échec par defaut)
-- video_resultat: caractères (10 max) qui dit si le test vidéo est ok ou échec (échec par defaut)
CREATE TABLE test (
  id_test INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  test_datetime DATETIME NOT NULL,
  operateur VARCHAR(30) NOT NULL,
  download DECIMAL(5,2) UNSIGNED NOT NULL,
  upload DECIMAL(5,2) UNSIGNED NOT NULL,
  debit_resultat VARCHAR(10) default 'echec',
  mirroir_resultat VARCHAR(10) default 'echec',
  video_resultat VARCHAR(10) default 'echec'
);

-- Création de la table localisation 
-- id: entier non signé qui s‘auto incrémente et il doit être unique
-- lat: float qui permet de récupérer la latitude
-- lng: float qui permet de récupérer la longitude
-- id_questionnaire: récupère l'id_questionnaire afin d'associer les tables entre elles
-- id_test:récupère l'id_test afin d'associer les tables entre elles
-- tout l'optionnel permet de récupérer les données de l'utilisateur nom,prenom, adresse, code postal et la ville
CREATE TABLE localisation (
  id_local INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  lat FLOAT NOT NULL,
  lng FLOAT NOT NULL,
  id_T INT UNSIGNED,
);

-- Contraintes FK
-- liaison des ids des tables questionnaire et test à la table localisation
-- ALTER TABLE table_name ADD CONSTRAINT fk_foreign_key_name FOREIGN KEY (foreign_key_name) REFERENCES target_table(target_key_name);
alter table localisation add constraint localisation_test_FK foreign key (id_T) references test(id_test);


-- Création de la table administrateur 
-- id: entier non signé qui s‘auto incrémente et il doit être unique
-- email: email de l'admin qui sera son login
-- password: est le mot de passe de l'admin
CREATE TABLE admin (
    id_admin INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(50) UNIQUE NOT NULL,
    password VARCHAR(50) NOT NULL
);